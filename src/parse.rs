use alloc::string::String;
use crate::gpio::spinny_leds;
use crate::gpio::*;
//use stm32f30x::{GPIOA,GPIOB,GPIOC,GPIOD,GPIOE};
use alloc::prelude::*;

//pretty self-explanitory
pub fn parse(s:&String,pins:&Pins) {
        let v:Vec<&str> = s.split(|c| c == ' ').collect();

        if v[0] == "spin" {
            spinny_leds(&pins.gpioe);
        }
        if v[0] == "on" {
            let p = v[1].chars().collect();
            set_pin(&pins,Pin::parse(p).unwrap());
        }
        if v[0] == "off" {
            let p = v[1].chars().collect();
            unset_pin(&pins,Pin::parse(p).unwrap());
        }

}
