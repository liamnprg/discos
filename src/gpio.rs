use stm32f30x::{GPIOA,GPIOB,GPIOC,GPIOD,GPIOE};
use cortex_m::asm;
use alloc::prelude::*;

pub trait Gpio {
    fn set_pin(&self,i:u8);
    fn unset_pin(&self,i:u8);
}
//The gpio's on the stm32
pub enum Block {
    A,B,C,D,E
}

//pb6
pub struct Pin {
    pub block:Block,
    pub pin:u32,
} 

//GPIOs
pub struct Pins {
    pub gpioa: GPIOA,
    pub gpiob: GPIOB,
    pub gpioc: GPIOC,
    pub gpiod: GPIOD,
    pub gpioe: GPIOE,
}
impl Pin {
    //parsing stuff like pb16 and pa9
    pub fn parse(s: Vec<char>) -> Option<Self> {
        let mut s = s.iter();
        //get rid of the p
        s.next()?;
        //get the a,b,c,d,e
        let blockch = s.next()?;
        //get the 1 in pb16
        let digit2=s.next()?;
        //get the 6 in pb16
        let digit1=s.next();

        let block = match blockch {
            'a' => Block::A,
            'b' => Block::B,
            'c' => Block::C,
            'd' => Block::D,
            'e' => Block::E,
            _ => (return None),
        };

       let d2=digit2.to_digit(10)?;
       let mut total=d2;

       if let Some(d1) = digit1 {
           //it will be in the 10's place now :)
           total=total*10+(d1.to_digit(10)?)
       }

       if total <= 15 {
            Some(Pin {
                block:block,
                pin:total,
            })
       } else {
           None
       }
    }
}

//implement gpio trait for gpios
macro_rules! gpio {
    ($x:ty) => (
            impl Gpio for $x {
                fn set_pin(&self,i:u8) {
                    unsafe {
                        self.moder.modify(|r,w| w.bits(r.bits() | 0b1<<(i*2)));
                        self.odr.modify(|r,w| w.bits(r.bits() | 0b1<<i))
                    }
                }
                fn unset_pin(&self,i:u8) {
                    unsafe {
                        self.bsrr.write(|w| w.bits(0b1<<(16+i)));
                    }
                }
            }
        );
}

gpio!(&GPIOA);
gpio!(&GPIOB);
gpio!(&GPIOC);
gpio!(&GPIOD);
gpio!(&GPIOE);



pub fn spinny_leds(gpioe:&GPIOE) {
    for x in 8..16 {
        //set_led(x,&gpioe);
        gpioe.set_pin(x);
        asm::delay(5000000)
    }

    // 8..16 stops at x < 16, so does not use x=16, 8..10=[8,9]
    for x in 8..16 {
        //unset_led(x,&gpioe);
        gpioe.unset_pin(x);
        asm::delay(5000000)
    }
}


pub fn set_pin(gpio:&Pins, pin: Pin) {
    match pin.block {
        Block::A => {
            (&gpio.gpioa).set_pin(pin.pin as u8);
        },
        Block::B => {
            (&gpio.gpiob).set_pin(pin.pin as u8);
        },
        Block::C => {
            (&gpio.gpioc).set_pin(pin.pin as u8);
        },
        Block::D => {
            (&gpio.gpiod).set_pin(pin.pin as u8);
        },
        Block::E => {
            (&gpio.gpioe).set_pin(pin.pin as u8);
        },
    };
}
pub fn unset_pin(gpio:&Pins, pin: Pin) {
    match pin.block {
        Block::A => {
            (&gpio.gpioa).unset_pin(pin.pin as u8);
        },
        Block::B => {
            (&gpio.gpiob).unset_pin(pin.pin as u8);
        },
        Block::C => {
            (&gpio.gpioc).unset_pin(pin.pin as u8);
        },
        Block::D => {
            (&gpio.gpiod).unset_pin(pin.pin as u8);
        },
        Block::E => {
            (&gpio.gpioe).unset_pin(pin.pin as u8);
        },
    };
}
