#![feature(custom_attribute)]
#![feature(alloc)]
#![feature(alloc_error_handler)]
#![feature(lang_items)]
#![no_main]
#![no_std]
 
extern crate cortex_m;
#[macro_use] 
extern crate cortex_m_rt as rt;
extern crate alloc;
extern crate panic_halt;
 
mod gpio;
mod parse;

use cortex_m::asm;
use stm32f30x::USART1;
use cast::u8;
use core::alloc::Layout;
use alloc_cortex_m::CortexMHeap;
//use self::alloc::vec;
use self::alloc::string::String;
use crate::gpio::*;
use crate::parse::parse;


#[global_allocator]
static ALLOCATOR: CortexMHeap = CortexMHeap::empty();

const HEAP_SIZE: usize = 1024; // in bytes

#[entry]
fn main() -> ! {
    let dp = stm32f30x::Peripherals::take().unwrap();
    // Initialize the allocator BEFORE you use it
    unsafe { ALLOCATOR.init(cortex_m_rt::heap_start() as usize, HEAP_SIZE) }

    
    //<setup>
    let gpioa = dp.GPIOA;
    let gpiob = dp.GPIOB;
    let gpioc = dp.GPIOC;
    let gpiod = dp.GPIOD;
    let gpioe = dp.GPIOE;

    let rcc = dp.RCC;
    let usart1 = dp.USART1;
    
    rcc.apb2enr.write(|w| w.usart1en().enabled());
    rcc.ahbenr.modify(|_, w| w.iopaen().enabled());
    rcc.ahbenr.modify(|_, w| w.iopben().enabled());
    rcc.ahbenr.modify(|_, w| w.iopcen().enabled());
    rcc.ahbenr.modify(|_, w| w.iopden().enabled());
    rcc.ahbenr.modify(|_, w| w.iopeen().enabled());


    gpioa.moder.modify(|_, w| w.moder9().alternate());
    gpioa.moder.modify(|_, w| w.moder10().alternate());

    unsafe {
        //test out zero extended at bottom and top
        gpioa.afrh.write(|w| w.afrh9().bits(0b0111).afrh10().bits(0b0111));
        usart1.brr.write(|w| w.bits(0x681));
    }

    usart1.cr1.write(|w| w.over8().set_bit().ue().set_bit().te().set_bit().re().set_bit());




    let pins: Pins = Pins {gpioa,gpiob,gpioc,gpiod,gpioe};
    //</setup>
    loop{
        //get a character
        
        let mut s = String::new(); 
        let mut done = false;
        while !done {
            let c = getch(&usart1);

            if c != '\n' {
                s.push(c)
            } else {
                parse(&s,&pins);
                putstr(&s);
                done = true;
            }

        }
    }
}

fn getch(usart1: &USART1) -> char {
    while !usart1.isr.read().rxne().bit_is_set() {}
    let c = usart1.rdr.read().bits();
    u8(c).unwrap() as char
}

fn putstr(s: &str) {
    let usart1 = USART1::ptr();
    for ch in s.chars() {
            unsafe {
                while !(*usart1).isr.read().tc().bit_is_set() {}
                (*usart1).tdr.write(|w| w.bits(ch as u32));
            }
    }
}

// define what happens in an Out Of Memory (OOM) condition
#[alloc_error_handler]
fn alloc_error(_layout: Layout) -> ! {
    asm::bkpt();

    loop {}
}
