# Discos

Not an actual operating system, and no disk support(yet), but the name was too cool to pass up.

Discovery+disc-os=discos!

## Actually using

use an stm32f303discovery board, and follow the instructions in the embedded-rust "the book" book. 

## Actually actually using

Supports three commands:

on(sets a gpio pin on)

off(turns that pin off)

spin(spinny led wheel).

on and off require a pin, so to set the first led pin on, use 

on pe8.

same goes with off

## Hardware needed.

You will need a ttl-to-usb converter with rx attached to pa10, and tx attached to pa9. 

## More features to be added later.

gyroscope/accel reading.

sd cards/floppy disks.


## A better version of this

Use monotron. 


## serial stuff

use a ttl-to-usb converter as afformentioned, in addition all text entered will need a \n at the end of it, otherwise nothing will be accepted. You can also use my serial program for interacting with the board. It is available here: https://gitlab.com/liamnprg/discos-serial

